<?php session_start();
header("Content-Type: text/html; charset=UTF-8");


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="http://code.jquery.com/jquery-latest.js"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Каталог - табличный вид</title>
</head>

<body>
    <div class="main">
        <div class="header">
            
            <a href="#"><img class="logo" src="images/logo.jpg" alt="Интернет магазин товаров ручной работы" /></a>
            <img class="slogan" src="images/slogan.jpg" alt="Интернет магазин товаров ручной работы" />   
            
            <div class="header_right">     
                <img class="info" src="images/info.jpg" alt="Информация" />  
                <div class="search-head">
                    <form method="get" action="">
                        <input class="search-text" type="text" name="search" placeholder="Что искать?" />
                        <input type="image" class="search-btn" src="images/search-btn.jpg" />
                    </form> 
                </div>
            </div>
        </div>
        
        <ul class="menu">
            <li><a href="http://test1.ru/index1.php">Главная</a></li>
            <li><a href="http://test1.ru/index-about.php">О магазине</a></li>
            <li><a href="http://test1.ru/index-help.php">Помощь</a></li>
            <li><a href="http://test1.ru/index-contact.php">Контакты</a></li> 				       			         		           
        </ul>   
            
        <div id="left-bar">
             <div class="left-bar-cont">			
               <?php
               include("catalog.php");
               ?>
                
                <div class="information">
                    <h2><span>Информация</span></h2>
                    <ul class="nav-information">
                        <li>- <a href="#">Новинки</a></li>
                        <li>- <a href="#">Популярное</a></li>
                        <li>- <a href="#">Как выбрать товар</a></li>
                        <li>- <a href="#">Как заказать товар</a></li>
                    </ul>
                </div>
            </div>
        </div>
            
        <div class="center">
            <div id="contentwrapper">
                <div id="content">            
                    <div class="catalog-index">
                     	<div class="kroshka">
                        <?php
                        
                        if (isset($_GET["db"]))
                        {
                            if ($_GET["db"] == "c")
                            {
                                
                                echo '<a href="#">'.$_GET["name"].'</a>';
                            }
                            if ($_GET["db"] == "t")
                            {
                                echo '<a href="#">По типу работы</a> / <span>'.$_GET["name"].'</span>';    
                            }
                            if ($_GET["db"] == "m") 
                            echo '<a href="#">По материалу</a> / <span>'.$_GET["name"].'</span>';
                            
                        }
                        ?>
                        </div>
                        
                        <div class="vid-sort">
                            Вид: 
                           <?php
                           echo '<a href="http://test1.ru/index-table.php?id='.$_GET["id"].'&db='.$_GET["db"].'&name='.$_GET["name"].'"><img src="images/table-on.jpg" title="табличный вид" alt="табличный вид" /></a>'; 
                           echo '<a href="http://test1.ru/index-line.php?id='.$_GET["id"].'&db='.$_GET["db"].'&name='.$_GET["name"].'"><img src="images/line-off.jpg" alt="линейный вид" /></a>';  
?>
                            &nbsp;           
                            Сортировать по:&nbsp;    
                                <a href="#" class="sort-bot-act">цене</a>  &nbsp;|&nbsp;     
                                <a href="#" class="sort-bot">названию</a>  &nbsp;|&nbsp;     
                                <a href="#" class="sort-bot">добавлению</a>
                        </div>
                        <?php                             
                   $db = connect_db();
                   $sqlType = null;
                   $sql = null;
                   //Запрос к промежуточным таблицам ItemMat и ItemType для получения id товаров, принадлежащим к данной категории
                   if (isset ($_GET["id"]) and (isset($_GET["db"])))
                         { 
                            if ($_GET["db"] == "t")
                            {
                                $query = "SELECT name, price, id FROM Items WHERE id IN (SELECT iid FROM ItemType WHERE tid=".$curId.")";
                                $sql = mysql_query($query, $db);
                                $sqlType = mysql_fetch_array($sql);
                                
                                
                            } 
                            if ($_GET["db"] == "m")
                            {
                                $query = "SELECT name, price, id FROM Items WHERE id IN (SELECT iid FROM ItemMat WHERE matid=".$curId.")";
                                $sql = mysql_query($query, $db);
                                $sqlType = mysql_fetch_array($sql);
                            }
                            if ($_GET["db"] == "c")
                            {
                                $query = "SELECT name, price, id FROM Items WHERE mainid=".$curId."";
                                $sql = mysql_query($query, $db);
                                $sqlType = mysql_fetch_array($sql);
                            }
                         }
                   $_SESSION["sql"] = $query;
                   
                   
                  // $_SESSION['item']=$array[1];
                   /**
 * echo "<HTML><HEAD> 
 *    <META HTTP-EQUIV='Refresh' CONTENT='0; URL=test.php?value=$array'> 
 *     </HEAD></HTML>"; 
 */
                   
                        if ($sql != null)
                        do
                        {
                            echo '<div class="product-table">';
                            echo '<h2><a href="http://test1.ru/index-detail.php?id='.$sqlType["id"].'">'.$sqlType["name"].'</a></h2>';
                            echo '<div class="product-table-img">   '    ;          
                                /*Получить картинку из бд с данным id*/
                            $sqlImage = mysql_query("SELECT url, name FROM Images WHERE iid=".$sqlType["id"]." AND icon=1", $db);
                            $image = mysql_fetch_array($sqlImage);
                            echo '<a href="http://test1.ru/index-detail.php?id='.$sqlType["id"].'"><img src="'.$image["url"].'" width="150" height="250" alt="'.$image["name"].'" /></a>';
                            echo '</div>';
                            echo '<p class="cat-table-more"><a href="http://test1.ru/index-detail.php?id='.$sqlType["id"].'">подробнее...</a></p>';
                            echo '<p>Цена :  <span>'.$sqlType["price"].'</span></p>';
                            echo '<div id="product-table-btn">
                                <a href="#">В избранное</a>
                            </div> ';
                            
                            
                        }
                        while($sqlType = mysql_fetch_array($sql));
                          
                        ?>            
                        
                        
                        </div> 
                        
                        <div class="clr"></div>
                        <div class="pager">
                            <a href="#"><img src="images/pager-prev.jpg" alt="назад" /></a>
                            1
                            <a href="#">2</a>  
                            <a href="#">3</a>  
                            <a href="#">4</a>  
                            <a href="#">5</a>  
                            <a href="#">...</a>  
                            <a href="#">27</a>
                            <a href="#"><img src="images/pager-next.jpg" alt="вперед" /></a>
                        </div>
                    </div>   
                </div>
           	</div>
        </div>
                
        <div id="right-bar">
            <div class="right-bar-cont">        	
                <div class="enter">
                    <h2><span>Авторизация</span></h2>
                    <div id="enter-btn">
                        <a href="#">Вход/Авторизация</a>
                    </div>
                </div>
                
                <div class="basket">
                    <h2><span>Избранное</span></h2>
                    <div>
                        <p>
                            У вас в избранном<br />
                            <span>x</span> товар на <span>xxxxx</span> руб
                        </p>
                        <div id="basket-btn">
                            <a href="#">Посмотреть избранное</a>
                        </div>
                    </div>
                </div>    
                        
                <?php
                include('search.php');
	
?>
            </div>
        </div>
        
        <div class="footer">
            <div class="flogo">
                <a href="/"><img src="images/footer-logo.png" alt="Интернет магазин сотовых телефонов" /></a>
                <p>Сopyright © 2014</p>
            </div>	
            <div class="finfo">
                <p>Все вопросы и предложения </p>
                <p>можно отправлять на почту:</p>
                <div id="e-mail">
                	<a href="#">Segyar@list.ru</a>
                </div>
            </div>
            <div class="fmenu">
                <p>Меню:</p>
                <ul class="ul1">
                    <li><a href="http://test1.ru/index1.php">Главная</a></li>                
                    <li><a href="http://test1.ru/index-help.php">Помощь</a></li>
                </ul>	
                <ul class="ul2">
                    <li><a href="http://test1.ru/index-about.php">О магазине</a></li>
                    <li><a href="http://test1.ru/index-contact.php">Контакты</a></li>
                </ul>
            </div>
        </div>      
    </div>
</body>
</html>