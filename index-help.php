﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="http://code.jquery.com/jquery-latest.js"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Помощь</title>
</head>

<body>
    <div class="main">
        <div class="header">
            
            <a href="#"><img class="logo" src="images/logo.jpg" alt="Интернет магазин товаров ручной работы" /></a>
            <img class="slogan" src="images/slogan.jpg" alt="Интернет магазин товаров ручной работы" />   
            
            <div class="header_right">     
                <img class="info" src="images/info.jpg" alt="Информация" />  
                <div class="search-head">
                    <form method="get" action="">
                        <input class="search-text" type="text" name="search" placeholder="Что искать?" />
                        <input type="image" class="search-btn" src="images/search-btn.jpg" />
                    </form> 
                </div>
            </div>
        </div>
        
        <ul class="menu">
            <li><a href="http://test1.ru/index1.php">Главная</a></li>
            <li><a href="http://test1.ru/index-about.php">О магазине</a></li>
            <li><a href="http://test1.ru/index-help.php">Помощь</a></li>
            <li><a href="http://test1.ru/index-contact.php">Контакты</a></li> 		       			         		           
        </ul>   
            
        <div id="left-bar">
             <div class="left-bar-cont">			
                <?php
                include("catalog.php");
                ?>
                
                <div class="information">
                    <h2><span>Информация</span></h2>
                    <ul class="nav-information">
                        <li>- <a href="#">Новинки</a></li>
                        <li>- <a href="#">Популярное</a></li>
                        <li>- <a href="#">Как выбрать товар</a></li>
                        <li>- <a href="#">Как заказать товар</a></li>
                    </ul>
                </div>
            </div>
        </div>
            
        <div class="center">
            <div id="contentwrapper">
                <div class="content-txt">
                    <h2>Помощь</h2>
                    <p>Здесь будет содержаться информация, помогающая пользователю сориентироваться на сайте</p>
                </div>
           	</div>
        </div>
                
        <div id="right-bar">
            <div class="right-bar-cont">        	
                <div class="enter">
                    <h2><span>Авторизация</span></h2>
                    <div id="enter-btn">
                        <a href="#">Вход/Авторизация</a>
                    </div>
                </div>
                
                <div class="basket">
                    <h2><span>Избранное</span></h2>
                    <div>
                        <p>
                            У вас в избранном<br />
                            <span>x</span> товар на <span>xxxxx</span> руб
                        </p>
                        <div id="basket-btn">
                            <a href="#">Посмотреть избранное</a>
                        </div>
                    </div>
                </div>    
                        
                 <?php
                include('search.php');
                ?>
            </div>
        </div>
        
        <div class="footer">
            <div class="flogo">
                <a href="/"><img src="images/footer-logo.png" alt="Интернет магазин сотовых телефонов" /></a>
                <p>Сopyright © 2014</p>
            </div>	
            <div class="finfo">
                <p>Все вопросы и предложения </p>
                <p>можно отправлять на почту:</p>
                <div id="e-mail">
                	<a href="#">Segyar@list.ru</a>
                </div>
            </div>
            <div class="fmenu">
                <p>Меню:</p>
                 <ul class="ul1">
                    <li><a href="http://test1.ru/index1.php">Главная</a></li>                
                    <li><a href="http://test1.ru/index-help.php">Помощь</a></li>
                </ul>	
                <ul class="ul2">
                    <li><a href="http://test1.ru/index-about.php">О магазине</a></li>
                    <li><a href="http://test1.ru/index-contact.php">Контакты</a></li>
                </ul>
            </div>
        </div>      
    </div>
</body>
</html>